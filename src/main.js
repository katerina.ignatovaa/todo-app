/**
 * Задание 1
 *
 * 1. Реализуем приложение Список дел с возможностью добавить новый элемент в список из инпута по нажатию кнопки "Добавить";
 * 2. При нажатии на галочку текст итема становится зачеркнутым, при отжатии снова обычным.
 * 3. Реализовать удаление элемента по нажатию крестика;
 * 4. Реализовать возможность редактирования элемента:
 * - При нажатии на текст элемента он отображается в инпуте, название кнопки меняется на "Обновить";
 * - После изменения названия и нажатия на "Обновить" текст элемента изменяется в списке, инпут сбрасывается, кнопка переименовывается обратно в "Добавить".;
 * - Предусмотреть возможность отменить редактирование (крестик в конце инпута), при нажатии на который редактирование прекращается: инпут очищается, кнопка вновь имеет заголовок "Добавить".
 * - Редактируемый элемент подсвечивается как активный.
 * 5. Сохрание текущего состояния (local storage).
 */

if(!localStorage.getItem('tasks')) {
    setTasksToLocalStorage();
}
else {
    getTasksFromLocalStorage();
}

document.querySelector('.btn-js').addEventListener(
    'click',
    function (event) {
        if (event.target.textContent !== 'Обновить'){
            const newTask = document.querySelector('.input-js').value;
            createNewTask(newTask);
            document.querySelector('.input-js').value = '';
            setTasksToLocalStorage();
        }
    }
)

document.querySelector('.list').addEventListener(
        'click',
        function (event) {
            if (event.target.classList.contains('checkbox-js')){
                event.target.parentNode.querySelector('.task-name').classList.toggle('done');
                setTasksToLocalStorage();
            }
        }
)

document.querySelector('.list').addEventListener(
        'click',
        function (event) {
            if (event.target.classList.contains('remove')){
                event.target.parentNode.remove(this);
                setTasksToLocalStorage();
            }
        }
)

document.querySelector('.list').addEventListener(
    'click',
    function (event) {
        if (event.target.classList.contains('task-name')){
            document.querySelector('.input-js').value = `${event.target.textContent}`;
            document.querySelector('.btn-js').textContent = 'Обновить';
            event.target.classList.add('active');
            const cancelEditing = document.createElement('button');
            cancelEditing.innerHTML = '&#215;';
            cancelEditing.classList.add('cancel');
            document.querySelector('.input-js').parentNode.append(cancelEditing);
            document.querySelector('.cancel').addEventListener(
                'click',
                function () {
                    document.querySelector('.active').classList.remove('active');
                    document.querySelector('.btn-js').textContent = 'Добавить';
                    document.querySelector('.input-js').value = '';
                    document.querySelector('.input-field').removeChild(cancelEditing);
                }
            )
            document.querySelector('.btn-js').addEventListener(
                'click',
                function (event) {
                    document.querySelector('.active').textContent = document.querySelector('.input-js').value;
                    document.querySelector('.active').classList.remove('active');
                    event.target.textContent = 'Добавить';
                    document.querySelector('.input-js').value = '';
                    document.querySelector('.input-field').removeChild(cancelEditing);
                    setTasksToLocalStorage();
                }
            )
        }
    }
)

function createNewTask(content){
    const newItem = document.createElement('li');
    newItem.innerHTML = `<div class="task"><input class="form-check-input checkbox-js" type="checkbox" value=""><span class="task-name">${content}</span></div><button class="remove">&#215;</button>`;
    newItem.classList.add('list-item');
    document.querySelector('.list').append(newItem);
}

function getTasksFromLocalStorage(){
    document.querySelector('.list').innerHTML = '';
    const tasks = localStorage.getItem('tasks').split(',');
    for(let i = 0; i < tasks.length; i++){
        if(tasks[i] !== ''){
            createNewTask(tasks[i]);
        }

    }
}

function setTasksToLocalStorage(){
    const tasks = [];
    document.querySelectorAll('.task-name').forEach(
        task => {
            if(!task.classList.contains('done')){
                return tasks.push(task.textContent);
            }
        }
    );
    localStorage.setItem('tasks', tasks.toString());
}
